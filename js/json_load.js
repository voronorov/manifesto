var data;
var href;
var lang;

href = $(location).attr('search').substr(1);

$.get(`manifesto/${href}.json`, function(d) {
  data = d;
  switch(href) {
    case 'en':
      en = '<b>EN</b>';
      ru = 'RU';
      zh = '漢語';
      break;
    case 'ru':  
      en = 'EN';
      ru = '<b>RU</b>';
      zh = '漢語';
      break;

    case 'zh':
      en = 'EN';
      ru = 'RU';
      zh = '<b>漢語</b>';
      break;      
    default:
      en = 'EN';
      ru = 'RU';
      zh = '漢語';     
      break;
  }
  lang = `<a href="/?en" hreflang="en" style="color: #fff">${en}</a> <a href="/?ru" hreflang="ru" style="color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${ru}</a><a href="/?zh" hreflang="zh-Hant" style="color: #fff">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${zh}</a>`; 
  
  $("#fullpage").append(`	<div class="section" id="section0"><div class="slide" id="slide0"><div class="intro"><h1>A{rch}gile Manifesto</h1><br><p>${lang}</p></div></div></div>`);   
  $.each(data.manifesto, function(key, value) {
    $("#fullpage").append(`	<div class="section" id="section0"><div class="intro"><p>${value}</p></div></div></div>`);
  });
});
