if (document.documentElement.clientWidth < 900){
  particlesJS.load('particles-js', 'json/particles_mobile.json', function() {
    console.log('callback - particles.js config loaded');
  });
}
  
else {
  particlesJS.load('particles-js', 'json/particles.json', function() {
    console.log('callback - particles.js config loaded');
  });
}      
